
;(function() {
  var swiper = new Swiper(".mySwiper", {
     slidesPerView: 3,
     spaceBetween: 30,
     pagination: {
       el: ".swiper-pagination",
       clickable: true,
     },
     breakpoints: {
       "@0.00": {
         slidesPerView: 1,
         spaceBetween: 10,
       },
       "@0.75": {
         slidesPerView: 2,
         spaceBetween: 20,
       },
       "@1.00": {
         slidesPerView: 3,
         spaceBetween: 30,
       },
       "@1.50": {
         slidesPerView: 3,
         spaceBetween: 30,
       },
      },
   });
})();
